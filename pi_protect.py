#-------------------------------------------------------------------------------------------------------------------#
# Ian Tyler                                                                                                         #
# Pi_Protect v1.0                                                                                                   #
# 26 pin GPIO RPi                                                                                                   #
# This program allows individuals to convert their Raspberry Pi into a home security system.                        #
#                                                                                                                   #
#                                                                                                                   #
# Features:                                                                                                         #
#    Motion Detection                                                                                               #
#    Integrated Camera with Picture and Video Capabilities                                                          #
#    RFID Scanner with "Random" Code Writer                                                                         #
#    LED System Indicator (Armed/Disarmed) Lights                                                                   #
#    Button Activated Arming System                                                                                 #
#    Automatic Email Intrusion Alert System with Photo and Video of intruder                                        #
#    Personalization of Email Message through Gmail SSMTP                                                           #
#                                                                                                                   #
# Included Files:                                                                                                   #                                                                                                 #
#    email_attach.py                                                                                                #
#    MFRC522.py                                                                                                     #
#    pi_protect.py                                                                                                  #                                                                                                    #                                                                                                  #
#    read.py                                                                                                        #                                                                                                   #
#    write.py                                                                                                       #
#    video.py                                                                                                       #
#    card_code_log.txt                                                                                              #
#    current_code.txt                                                                                               #
#    system_log.txt                                                                                                 #
#    README.md                                                                                                      #                                                                                      #
#    LICENSING.md                                                                                                   #
#                                                                                                                   #
# Reference Documents Folder:                                                                                       #
#    Pi_Protect Logo                                                                                                #
#    Wiring Diagram this version of Pi_Protect                                                                      #
#    Wiring Diagram text for this version of Pi_Protect                                                             #
#                                                                                                                   #
#                                                                                                                   #
# For more information about setup or to contact the writer, see: http://www.ianglentyler.com                       #
#                                                                                                                   #
# Information about this project is contained under the PI_PROTECT navigation tab                                   #
#                                                                                                                   #
#-------------------------------------------------------------------------------------------------------------------#


# Imported modules and functions
import RPi.GPIO as GPIO
import datetime
import random
import video
import time
import sys
import os
import re

from email_attach import *
from read import *

# Prevent GPIO errors
GPIO.setwarnings(False)                                                 # This disables the GPIO warning messages
GPIO.cleanup()                                                          # Used here as well as the end to make sure GPIO "slate" is clean

# Set-up GPIO board and pins
GPIO.setmode(GPIO.BOARD)                                                # Sets GPIO mode to "BOARD" this is the inner numbering on the diagram (pin #'s 1-26)
GPIO.setup(12, GPIO.IN, pull_up_down=GPIO.PUD_UP)                       # Input for Arm button
GPIO.setup(7, GPIO.IN)                                                  # Input for Motion detector
GPIO.setup(18, GPIO.OUT)                                                # Red LED armed
GPIO.setup(16, GPIO.OUT)                                                # Green LED DIS-armed


# Initialize lights to "disarmed" position
GPIO.output(16, True)
GPIO.output(18, False)

# Initialization of loop variables
done = False
armed = False

# This is for cycling through video files for conversion/email_attachment
file_type = ['.mp4', '.h264', '.jpg']


# Main body loop
while (done == False):

        # Sets button input for "Arm-system" button
        system_arm = GPIO.input(12)
        time.sleep(.3) # <-- "bounceback"
	
        # If "Arm-system" button is pressed
        if (system_arm == False):
                
                try:
                        
                        # Adds "System-Armed" event to log file, or catches error
                        try:
                                ts = time.time()
                                st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
                                with open("system_log.txt", "ab") as logfile:
                                        logfile.write("\nSystem armed at: %s" % (st))
                        except:
                                print "\nUnexpected error occurred:", sys.exc_info()[0]
                                

                        armed = True 	# Sets armed value for security monitoring
                        time.sleep(30)  # <-- Change this number to lengthen/shorten time until system arm
                                           # *Keep in mind that the person arming the system will need to get out of range, so allow some time

                except:
                        # Get error message
                        print "\nUnexpected error occurred:", sys.exc_info()[0]
                        

                # While system is "Armed"
                while (armed == True):						
                        
                        # Displays armed indicator light
                        GPIO.output(16, False)
                        GPIO.output(18, True)
                        print ("\nSystem armed.")


                        try:
                                # Waits for motion detection
                                GPIO.wait_for_edge(7, GPIO.RISING)
                                print ("\nMotion is detected!")

                                # Motion detected, checks for RFID
                                rfid_detected = (read())

                                # If RFID card has valid code, disarm the system
                                if (rfid_detected == True):
                                        
                                        # Disarms system at scan
                                        armed = False
                                        
                                        # Adds "System-Disarmed by: name" to event log
                                        try:
                                                ts = time.time()
                                                st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
                                                with open("system_log.txt", "ab") as logfile:
                                                        logfile.write("\nSystem disarmed at: %s\nBy: Ian" % (st)) # <-- Obviously you will want to change the name here
                                                print("\nSystem successfully disarmed")                            # (Or maybe you want my name disarming your security system, your choice)

                                        except:
                                                print "\nUnexpected error occurred:", sys.exc_info()[0]

                                        # Displays disarmed light
                                        GPIO.output(18, False)
                                        GPIO.output(16, True)


                                # If RFID is not scanned, or code is incorrect
                                elif (rfid_detected == False):

                                        # Cleans out previous videos from folder to only send new ones
                                        folder = '/home/pi/pi_protect-v1.0'
                                        for filename in os.listdir(folder):
                                                if os.path.exists(filename):
                                                        if filename.endswith(tuple(file_type)):
                                                                file_path = os.path.join(folder, filename)
                                                                try:
                                                                        if os.path.isfile(file_path):
                                                                                os.unlink(file_path)
                                                                except:
                                                                        print "\nUnexpected error occurred:", sys.exc_info()[0]

                                                
                                        # Takes a photo for email
                                        print ("\nTaking picture")
                                        video.picture()

                                        # Takes a video for email
                                        print ("\nTaking video")  
                                        video.video()

                                        # Emails alert of intrusion
                                        print ("\nSending email")

                                        
                                                # Fill this section with strings corresponding to the "createEmail" arguments #
                                        try:    #-----------------------------------------------------------------------------#
                                                sendEmail(createEmail('Receipient', 'Subject', 'Body of Message'))
                                                #-----------------------------------------------------------------------------#
                                                print ("\nEmail alert successfully sent!")
                                                
                                        # This error is returned if the video file cannot be found/attached correctly
                                        except (NameError,),e:
                                                print re.findall("name '(\w+)' is not defined", str(e))[0]
                                                                        
                                        # Adds event to log file
                                        try:
                                                ts = time.time()
                                                st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
                                                with open("system_log.txt", "ab") as logfile:
                                                        logfile.write("\n\nALERT: SOMEONE IS HERE!\nAt: %s\n" % (st)) # <-- You can customize the log entry by changing this string
                                                print("\nSystem log updated.")
                                                
                                        except:
                                                print "\nUnexpected error occurred:", sys.exc_info()[0]


                                        # Keeps system armed
                                        armed = True
                                        done = False
                                        time.sleep(30)  # <-- Seconds until system tries to detect motion again, this should reduce the number of duplicate alerts


                # Allows keyboard interrupt to break out of code and end program		
                except KeyboardInterrupt:
                        done = True
                        GPIO.cleanup()
                        exit()

                        
                
# Ends program
GPIO.cleanup()
exit()

