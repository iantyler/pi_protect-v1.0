#!/usr/bin/env python
# -*- coding: utf8 -*-
# Ian Tyler
# Pi_Protect RFID reader module
# This module allows the system to read and authenticate an RFID card's code
# This allows the system to disarm on a correct code, or trigger the alarm on an incorrect one (or non-read in specified time)
# Original module obtained from: https://github.com/mxgxw/MFRC522-python under MIT license and modified for individual use

# Import modules and functions
import RPi.GPIO as GPIO
import MFRC522
import signal
import time
import sys


# Capture SIGINT for clean-up when the script is aborted
def end_read(signal,frame):
    global continue_reading
    print "Ctrl+C captured, ending read."
    continue_reading = False
    
# Function to read RFID card
def read():
        
    continue_reading = True
    timer = 5      # <------------------------------------------------------ Change this to the number of seconds to scan for RFID before triggering alarm
                        # *Note: The shorter the time, the faster the alarm response and video will occur

    # Hook the SIGINT
    signal.signal(signal.SIGINT, end_read)

    # Create an object of the class MFRC522
    MIFAREReader = MFRC522.MFRC522()

    # Welcome message
    print "\nChecking for card read...\n"


    # This loop keeps checking for chips. If one is near it will get the UID and authenticate
    while (continue_reading == True):
		
	    # Scan for cards    
	    (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)

	    # If a card is found
	    if status == MIFAREReader.MI_OK:
		print "Card detected"
		
                # Get the UID of the card
                (status,uid) = MIFAREReader.MFRC522_Anticoll()

	    # If we have the UID, continue
            if status == MIFAREReader.MI_OK:

                # Print UID
		print "Card read UID: "+str(uid[0])+","+str(uid[1])+","+str(uid[2])+","+str(uid[3])
		
		# This is the default key for authentication
		key = [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]
			
		# Select the scanned tag
		MIFAREReader.MFRC522_SelectTag(uid)

		# Authenticate
		status = MIFAREReader.MFRC522_Auth(MIFAREReader.PICC_AUTHENT1A, 8, key, uid)

		# Check if authenticated
		if status == MIFAREReader.MI_OK:

		    # Opens current_code file for comparison
		    try:
                        code_file = open("current_code.txt", "r")
                        code = code_file.read()
		    except:
			print "Unexpected error occurred:", sys.exc_info()[0]

		    # Reads RFID card sector 8 code to variable for comparison	
                    card = MIFAREReader.MFRC522_Read(8)

                    # Compares card code to current_code file
                    # Sets val to "True" if card is verified (correct code)
		    if (code == card):
                        MIFAREReader.MFRC522_StopCrypto1()
                        continue_reading = False
                        val = True
                        break

		    # Sets val to "False" if card code is invalid/incorrect
                    else:
                        print "Authentication error"
                        MIFAREReader.MFRC522_StopCrypto1()
                        continue_reading = False
                        val = False
                                        
	    # Counter to allow time for scan	    
            time.sleep(1)
	    timer -= 1

	    # Once timer runs out, val is set to "False" and the read is ended
	    if (timer <= 0):
                MIFAREReader.MFRC522_StopCrypto1()
                continue_reading = False
                val = False

    # Returns authentication value  
    return val
				

