# Ian Tyler
# Pi_Protect email module
# This module allows the RPi to send an email with picture and video attachments through a ssmtp gmail server.
# If you encounter any difficulty, please see the tutorial at: https://community.webfaction.com/questions/2065/sending-email-attachments-using-python-not-sure-how-to

# Import modules and functions
import smtplib
from os.path import basename
import mimetypes
import email
import email.mime.application
import email.mime.base
from email import Encoders
import sys
import os

# Sets path for pictures and videos to attach
PATH = '/home/pi/pi_protect-v1.0'


# Creates email using mimetypes, returns message for send function
# The three parameters "to", "subject" and "body" will be used to fill corresponding spaces in the email
def createEmail(to, subject, body):

    # Creates mimed multipart email
    msg = email.mime.Multipart.MIMEMultipart()

    # Assigns parameters (and custom "from") and attaches to email
    msg['To'] = to
    msg['Subject'] = subject
    msg['From'] = "Ian's RPi <ian@gmail.com>"     # <---------------------------------------- Change this string to how you want the "from" address to appear
    body = email.mime.Text.MIMEText(body)
    msg.attach(body)

    # This function searches the established path for all files and directories
    # It then attaches all files starting with "20", as all the videos and pictures are timestamped with the year first
    for path, dirs, files in os.walk(PATH):
        for filename in files:
            if os.path.exists(filename):
                if filename.startswith("20"):
                    fullpath = os.path.join(path, filename)
                    with open(fullpath, 'rb') as f:
                        part = email.mime.base.MIMEBase('application', "octet-stream")
                        part.set_payload( open(filename, "rb").read() )
                        Encoders.encode_base64(part)
                        part.add_header('Content-Disposition', 'attachment; filename="%s"'
                                        % os.path.basename(fullpath))
                        msg.attach(part)

    # This returns the completed multi-part message for sending                             
    return msg;



# This function uses the ssmtp server we made in the setup section of my webpage
# It takes a message as a parameter, then sends it through Gmail using the appropriate credentials
def sendEmail(msg):
    s = smtplib.SMTP('smtp.gmail.com:587')
    s.starttls()
    s.login('GMAIL_NAME@gmail.com', 'GMAIL_PASSWORD')     # <------------------------------------------- Change these two strings to your Gmail information
    s.sendmail(msg['From'], msg['To'], msg.as_string())
    s.quit()


