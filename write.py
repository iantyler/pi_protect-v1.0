#!/usr/bin/env python
# -*- coding: utf8 -*-
# Ian Tyler
# Pi_Protect RFID code writer
# This module allows users to can an RFID card and write a "random" code to it.
# This code will be saved in a file for verification during the "read" function.

# Original module obtained from: (https://github.com/mxgxw/MFRC522-python) and modified for individual use

# Module imports
import RPi.GPIO as GPIO
import datetime
import MFRC522
import random
import signal
import time
import sys
import os


# Initialization of variables
continue_reading = True
change = 1
ts = time.time()

# Capture SIGINT for cleanup when the script is aborted
def end_read(signal,frame):
    global continue_reading
    print "Ctrl+C captured, ending read."
    continue_reading = False
    GPIO.cleanup()

# Hook the SIGINT
signal.signal(signal.SIGINT, end_read)

# Create an object of the class MFRC522
MIFAREReader = MFRC522.MFRC522()

# This loop keeps checking for chips. If one is near it will get the UID and authenticate
while continue_reading:
    
    # Scan for cards    
    (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)

    # If a card is found
    if status == MIFAREReader.MI_OK:
        print "Card detected"
    
    # Get the UID of the card
    (status,uid) = MIFAREReader.MFRC522_Anticoll()

    # If we have the UID, continue
    if status == MIFAREReader.MI_OK:

        # Print UID
        card_id = (str(uid[0])+", "+str(uid[1])+", "+str(uid[2])+", "+str(uid[3]))
        print "Card read UID: "+ card_id
    
        # This is the default key for authentication
        key = [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]
        
        # Select the scanned tag
        MIFAREReader.MFRC522_SelectTag(uid)

        # Authenticate
        status = MIFAREReader.MFRC522_Auth(MIFAREReader.PICC_AUTHENT1A, 8, key, uid)
        print "\n"

        # Check if authenticated
        if status == MIFAREReader.MI_OK:

            # Variable for the data to write
            data = []
            output = ""

            # Check owner of card
            if (card_id == "234, 91, 240, 85"): # <-- For card id, run "read.py" and scan the card. Change this id to your actual id.
                name = "Ian"    # <--Change "Ian" to desired name
            else:
                name = "Unidentified"

            #--------------------------------------------------------------------------------------------------------------------------------#
            # This formatting is important, so while you can change the values do NOT change the formatting/way it is entered into the output#
            #--------------------------------------------------------------------------------------------------------------------------------#
            
            # Creates formatted output for code comparison when reading RFID
            output += "["

            # Fill the data with a 'random' input
            for x in range(0,16):
                change += (random.randrange(0, 20))     # <-- These values can be changed for customization, but they work well enough as is
                data.append((0xEF) - change)        
                if (x < 15):
                    output += (str((0xEF) - change) + ", ")
                if (x >= 15):
                    output += (str((0xEF) - change))
                
            output += "]"
            #--------------------------------------------------------------------------------------------------------------------------------#
            
            # Formatted time-stamp
            st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
            
            # Adds write event to card_code backlog (as a failsafe)
	    try:
                with open("card_code_log.txt", "ab") as logfile:
		    logfile.write("\n%s's Card\nCard values: %s \nAs of: %s\n" % (name, output, st))	
	    except:
		print "Unexpected error occurred:", sys.exc_info()[0]



	    # Adds event to pi_protect system log
	    try:
                st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
		with open("system_log.txt", "ab") as logfile:
		    logfile.write("\n%s's card id updated: %s\n" % (name, st))	    
	    except:
                print "Unexpected error occurred:", sys.exc_info()[0]



	    # Adds current card code to file for verification during read cycle
	    try:
		st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
		with open("current_code.txt", "w") as logfile:
			logfile.write(output)   
	    except:
                print "Unexpected error occurred:", sys.exc_info()[0]


            # Read block 8 (Where our "code" will be stored)
            print "Sector 8 previous values:"
            MIFAREReader.MFRC522_Read(8)
            print "\n"

            # Write the new code to the card
            MIFAREReader.MFRC522_Write(8, data)
            print "\n"

            # Check to see if it was written
            print "Updated values:"
            MIFAREReader.MFRC522_Read(8)
            print "\n"

            # Stop
            MIFAREReader.MFRC522_StopCrypto1()

            # Make sure to stop reading for cards
            continue_reading = False

        # Error reading 
        else:
            print "Authentication error"
            continue_reading = False
            
GPIO.cleanup()
exit()
