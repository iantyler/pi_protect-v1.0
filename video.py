# Ian Tyler
# Pi_Protect video/picture module
# This module allows the RPi to take pictures of video and save them to the "current" directory

# Import modules and functions
import picamera
import datetime
import shutil
import time
import sys
import os


# This function takes a video for a specified amount of time and save it to the "current" directory
def video():

    # Initialize the camera object and set resoultion for video
    camera = picamera.PiCamera()
    camera.resolution = (640, 480)

    # For formatted time-stamp name
    ct = time.time()
    tim = datetime.datetime.fromtimestamp(ct).strftime('%Y_%m_%d_at_%H_%M_%S')

    # Video files from the camera are automatically ".h264" format, so it needs to be in the name
    name = (tim + '.h264')

    # Starts recording video with file named with formatted time-stamp and file type
    camera.start_recording(name)
    camera.wait_recording(15)   # <--------------------------------------------------------------- Change this number to set the number of seconds to record
    camera.stop_recording()         # *Remeber a shorter video will result in a faster alert email and have better chances of succeding in the email attachment process

    # The camera object MUST be closed, otherwise it will keep running and won't boot up again until manually killed or a system reboot
    camera.close()

    # This cycles through all files and directories in the "current" directory
    # It then creates an MP4 box for the video files, and deletes the original
    # This makes the video viewable on far mroe consoles (even though it is still technically a .h264 video)
    sfile = name.replace(".h264", "")   # <-- This changes the name to remove the tag and prevent double tags soon
    folder = '/home/pi/pi_protect-v1.0/'
    for filename in os.listdir(folder):
        if os.path.exists(filename):
            if filename.endswith(".h264"):
                os.system("MP4Box -add %s %s.mp4" % (filename, sfile))  # <-- This runs a terminal line from python that creates the MP4 box


                #--------------------This attempts to eliminate any .h264 files left after conversion--------------------#
                if filename.endswith('.h264'):
                    file_path = os.path.join(folder, filename)
                    try:
                        if os.path.isfile(file_path):
                            os.unlink(file_path)        
                    except:
                        print "\nUnexpected error occurred:", sys.exc_info()[0]
                #--------------------------------------------------------------------------------------------------------#



# This taks a picture and saves the file to the "current" directory
def picture():

    # Again, we must initialize a camera object and set resolution
    camera = picamera.PiCamera()
    camera.resolution = (1024, 768)

    # For formatted time-stamp name
    ct = time.time()
    tim = datetime.datetime.fromtimestamp(ct).strftime('%Y_%m_%d_at_%H_%M_%S')
    name = (tim + '.jpg')

    # Captures picture as jpg file with time-stamp as name, then closes camera
    camera.capture(name)
    camera.close()
